﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleDemo
{
    class Mummy : Monster
    {
        public Mummy()
        {
            Name = "The Mummy";
            Health = 120;
            Strength = 20;
        }
        public override int Health { get; set; }

        public override string Name { get; set; }

        public override int Strength { get; set; }

        public override string Attack(Player pl)
        {
            //int attackPow = rng.Next(Strength / 2, Strength);

            int attackDrain = rng.Next(Strength / 2, Strength);

            //pl.Health = pl.Health - attackPow;

            pl.Stamina = pl.Stamina - attackDrain;

            return "Mistook " + pl.Name + " for another mummy and tangled them up for " + attackDrain + " stamina!";
        }
    }
}
