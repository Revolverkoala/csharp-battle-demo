﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleDemo
{
    internal abstract class Monster : BattleEntity
    {
        public abstract string Name { get; set; }
        public abstract int Health { get; set; }
        public abstract int Strength { get; set; }

        public abstract string Attack(Player pl);
        public override string ToString()
        {
            return String.Format("{0,12} {1,3}", Name, Health);
        }
    }
}
