﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleDemo
{
    class Player : BattleEntity
    {
        private int _numAttacks;

        public Player()
        {
            Name = "Lou Costello";
            Health = 200;
            Stamina = 100;
            Strength = 20;
        }

        public string Name { get; set; }
        public int Health { get; set; }
        public int Stamina { get; set; }
        public int Strength { get; set; }
        //Here's an example of a calculated property
        //There's no field for IsAlive; it's calculated based on the player's health
        //each time the accessor is called
        public bool IsAlive {
            get {
                return (Health > 0);
            }
        }

        public int NumAttacks
        {
            get { return _numAttacks; }
        }

        public string Attack(Monster m)
        {
            int attackPow = rng.Next(Strength / 2, Strength);

            m.Health = m.Health - attackPow;

            return "Parodied " + m.Name + " for " + attackPow + " damage!";
        }
        public void newTurn()
        {
            _numAttacks = rng.Next(1, (Stamina / 10));
        }
    }
}
