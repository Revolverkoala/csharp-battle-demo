﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleDemo
{
    class WolfMan : Monster
    {
        public WolfMan()
        {
            Health = 80;
            Name = "Wolf Man";
            Strength = 40;
        }

        public override int Health { get; set; }

        public override string Name { get; set; }

        public override int Strength { get; set; }

        public bool InWerewolfForm
        {
            get {
                var now = DateTime.Now;

                //True if it's night, roughly
                return now.Hour <= 6 || now.Hour >= 21;
            }
        }

        public override string Attack(Player pl)
        {
            int attackPow = rng.Next(Strength / 2, Strength);

            pl.Health = pl.Health - attackPow;

            return "Tried to shred " + pl.Name + " for " + attackPow + " aggro points!";
        }
    }
}
