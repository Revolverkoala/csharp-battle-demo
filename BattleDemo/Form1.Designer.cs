﻿namespace BattleDemo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstMonsterList = new System.Windows.Forms.ListBox();
            this.lblMonsterList = new System.Windows.Forms.Label();
            this.btnAttack = new System.Windows.Forms.Button();
            this.btnHealthPotion = new System.Windows.Forms.Button();
            this.btnStaminaPotion = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNumAttacks = new System.Windows.Forms.Label();
            this.txtMonsterTurn = new System.Windows.Forms.TextBox();
            this.lblMonsterTurn = new System.Windows.Forms.Label();
            this.btnEndTurn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstMonsterList
            // 
            this.lstMonsterList.FormattingEnabled = true;
            this.lstMonsterList.Location = new System.Drawing.Point(12, 39);
            this.lstMonsterList.Name = "lstMonsterList";
            this.lstMonsterList.Size = new System.Drawing.Size(319, 95);
            this.lstMonsterList.TabIndex = 0;
            // 
            // lblMonsterList
            // 
            this.lblMonsterList.AutoSize = true;
            this.lblMonsterList.Location = new System.Drawing.Point(13, 20);
            this.lblMonsterList.Name = "lblMonsterList";
            this.lblMonsterList.Size = new System.Drawing.Size(64, 13);
            this.lblMonsterList.TabIndex = 1;
            this.lblMonsterList.Text = "Monster List";
            // 
            // btnAttack
            // 
            this.btnAttack.Location = new System.Drawing.Point(12, 140);
            this.btnAttack.Name = "btnAttack";
            this.btnAttack.Size = new System.Drawing.Size(75, 39);
            this.btnAttack.TabIndex = 2;
            this.btnAttack.Text = "Attack";
            this.btnAttack.UseVisualStyleBackColor = true;
            // 
            // btnHealthPotion
            // 
            this.btnHealthPotion.Location = new System.Drawing.Point(94, 140);
            this.btnHealthPotion.Name = "btnHealthPotion";
            this.btnHealthPotion.Size = new System.Drawing.Size(75, 39);
            this.btnHealthPotion.TabIndex = 3;
            this.btnHealthPotion.Text = "Health Potion";
            this.btnHealthPotion.UseVisualStyleBackColor = true;
            // 
            // btnStaminaPotion
            // 
            this.btnStaminaPotion.Location = new System.Drawing.Point(175, 140);
            this.btnStaminaPotion.Name = "btnStaminaPotion";
            this.btnStaminaPotion.Size = new System.Drawing.Size(75, 39);
            this.btnStaminaPotion.TabIndex = 4;
            this.btnStaminaPotion.Text = "Stamina Potion";
            this.btnStaminaPotion.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 182);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Player Health: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 201);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Player Stamina: ";
            // 
            // lblNumAttacks
            // 
            this.lblNumAttacks.AutoSize = true;
            this.lblNumAttacks.Location = new System.Drawing.Point(13, 224);
            this.lblNumAttacks.Name = "lblNumAttacks";
            this.lblNumAttacks.Size = new System.Drawing.Size(70, 13);
            this.lblNumAttacks.TabIndex = 7;
            this.lblNumAttacks.Text = "Attacks Left: ";
            // 
            // txtMonsterTurn
            // 
            this.txtMonsterTurn.Location = new System.Drawing.Point(12, 256);
            this.txtMonsterTurn.Multiline = true;
            this.txtMonsterTurn.Name = "txtMonsterTurn";
            this.txtMonsterTurn.ReadOnly = true;
            this.txtMonsterTurn.Size = new System.Drawing.Size(319, 98);
            this.txtMonsterTurn.TabIndex = 8;
            // 
            // lblMonsterTurn
            // 
            this.lblMonsterTurn.AutoSize = true;
            this.lblMonsterTurn.Location = new System.Drawing.Point(100, 240);
            this.lblMonsterTurn.Name = "lblMonsterTurn";
            this.lblMonsterTurn.Size = new System.Drawing.Size(73, 13);
            this.lblMonsterTurn.TabIndex = 9;
            this.lblMonsterTurn.Text = "Monster Turn:";
            // 
            // btnEndTurn
            // 
            this.btnEndTurn.Location = new System.Drawing.Point(256, 140);
            this.btnEndTurn.Name = "btnEndTurn";
            this.btnEndTurn.Size = new System.Drawing.Size(75, 39);
            this.btnEndTurn.TabIndex = 10;
            this.btnEndTurn.Text = "End Turn";
            this.btnEndTurn.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 366);
            this.Controls.Add(this.btnEndTurn);
            this.Controls.Add(this.lblMonsterTurn);
            this.Controls.Add(this.txtMonsterTurn);
            this.Controls.Add(this.lblNumAttacks);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnStaminaPotion);
            this.Controls.Add(this.btnHealthPotion);
            this.Controls.Add(this.btnAttack);
            this.Controls.Add(this.lblMonsterList);
            this.Controls.Add(this.lstMonsterList);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstMonsterList;
        private System.Windows.Forms.Label lblMonsterList;
        private System.Windows.Forms.Button btnAttack;
        private System.Windows.Forms.Button btnHealthPotion;
        private System.Windows.Forms.Button btnStaminaPotion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNumAttacks;
        private System.Windows.Forms.TextBox txtMonsterTurn;
        private System.Windows.Forms.Label lblMonsterTurn;
        private System.Windows.Forms.Button btnEndTurn;
    }
}

