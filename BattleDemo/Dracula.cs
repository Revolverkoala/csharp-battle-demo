﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleDemo
{
    class Dracula : Monster
    {
        public Dracula()
        {
            Health = 40;
            Name = "Dracula";
            Strength = 20;
        }
        public override int Health { get; set; }

        public override string Name { get; set; }

        public override int Strength { get; set; }

        public override string Attack(Player pl)
        {
            int attackPow = rng.Next(Strength / 2, Strength);

            int attackDrain = rng.Next(Strength / 4, Strength / 2);

            pl.Health = pl.Health - attackPow;

            pl.Stamina = pl.Stamina - attackDrain;

            return "Put " + pl.Name + " in a trance for " + attackPow + " bite points!\n"
                + "Also drained " + attackDrain + " stamina points!";
        }
    }
}
